package com.ncitguys.examples;

import lombok.Getter;
import lombok.Setter;

import com.ncitguys.db.common.DbCommon;
import com.ncitguys.db.common.annotations.DbColumn;
import com.ncitguys.db.common.annotations.DbTable;
import com.ncitguys.db.common.annotations.Index;
import com.ncitguys.db.common.annotations.Indexes;
import com.ncitguys.db.common.annotations.PrimaryKey;

@Getter
@Setter
@DbTable(name="testtable")
public class TestTable extends DbCommon<TestTable>{
	
	@DbColumn(autoIncrement=true)
	@PrimaryKey
	Integer testId;
	
	@DbColumn
	@Indexes(value={@Index(name="text_idx_1", columnOrder=1), @Index(name="text_idx_2", sortOrder=Index.SortOrder.DESCENDING)})
	String testString;
	
	@DbColumn
	@Index(name="text_idx_1", columnOrder=0)
	Integer testInt;
	
	@DbColumn
	Boolean testBool;
	
	
	public TestTable() {
		this(null);
	}
	
	public TestTable(String alias) {
		super(alias);
	}

	
	
}
