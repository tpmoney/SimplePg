package com.ncitguys.helperobjects;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class PhoneNumber implements Serializable {

	private static final long serialVersionUID = 1L;

	//Pattern from NANP http://en.wikipedia.org/wiki/North_American_Numbering_Plan
	public static Pattern NUMBER_PATTERN = Pattern.compile("^\\s*" //Arbitrary white space
			+ "\\(?([2-9][0-9][0-9]){1}?\\)?" //Area code, with or without parenthesis (mandatory)
			+ "[\\s\\-\\.\\/]*" //Arbitrary separators (white spaces, -'s, .'s or /'s)
			+ "([2-9](?:(?:[0-9&&[^1]][0-9])|(?:[0-9][0-9&&[^1]]))){1}?" //Exchange (mandatory)
			+ "[\\s\\-\\.\\/]*" //Arbitrary separators
			+ "([0-9]{4}?){1}?" //Subscriber code (mandatory)
			+ "[\\s\\-\\.\\/]*" //Arbitrary separators
			+ "(?:(?:e?xt?(?:ension)?)|\\#)?" //Any of ex, x, xt, ext, extension or #
			+ "[\\s\\-\\.\\/]*" //Arbitrary separators
			+ "(\\d+)?" //Any number of digits (once or not at all)
			+ "\\s*$"); //Arbitrary white space
	
	@Setter(AccessLevel.PROTECTED)
	String areaCode = null;
	@Setter(AccessLevel.PROTECTED)
	String exchange = null;
	@Setter(AccessLevel.PROTECTED)
	String subscriberCode = null;
	@Setter(AccessLevel.PROTECTED)
	String extension = null;
	
	private PhoneNumber(){
		
	}
	
	public static PhoneNumber parse(Object number){
		if(number == null){
			return null;
		}
		Matcher m = NUMBER_PATTERN.matcher(number.toString());
		if(m.matches()){
			PhoneNumber pn = new PhoneNumber();
			pn.setAreaCode(m.group(1));
			pn.setExchange(m.group(2));
			pn.setSubscriberCode(m.group(3));
			pn.setExtension(m.group(4));
			return pn;
		} else {
			return null;
		}
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		sb.append(areaCode);
		sb.append(")");
		sb.append(" ");
		sb.append(exchange);
		sb.append("-");
		sb.append(subscriberCode);
		if(extension != null){
			sb.append("x");
			sb.append(extension);
		}
		return sb.toString();
	}
}
