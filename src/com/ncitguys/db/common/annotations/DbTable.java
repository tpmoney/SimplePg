package com.ncitguys.db.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Annotation DbColumn.<br/>
 * This marks a class as a database table
 * 
 * @author Tevis Money
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface DbTable {
	
	/**
	 * Name.<br/>
	 * The name of this table in the database.
	 *
	 * @return the table column name
	 */
	String name();
	
	/**
	 * Column prefix.<br/>
	 * A prefix to append to all column names when interacting with the database
	 *
	 * @return the string
	 */
	String columnPrefix() default "";
	
	/**
	 * Schema.<br/>
	 * The schema that this table is in.
	 *
	 * @return the string
	 */
	String schema() default "";
}
