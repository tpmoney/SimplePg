package com.ncitguys.db.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Annotation Index.<br/>
 * This indicates that a column field in a database table is included in an index
 * 
 * @author Tevis Money
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Index {
	
	public static enum SortOrder{
		DEFAULT(""),
		ASCENDING(" ASC "),
		DESCENDING(" DESC ");
		
		String sqlValue = "";
		
		SortOrder(String sqlValue){
			this.sqlValue = sqlValue;
		}
		
		public String getSqlValue(){
			return sqlValue;
		}
	}
	
	public static enum NullOrder{
		DEFAULT(""),
		FIRST(" NULLS FIRST "),
		LAST(" NULLS LAST ");
		
		String sqlValue = "";
		
		NullOrder(String sqlValue){
			this.sqlValue = sqlValue;
		}
		
		public String getSqlValue(){
			return sqlValue;
		}
	}
	
	/**
	 * Name.<br/>
	 * The name of the index this column is included in. 
	 *
	 * @return the name of the index to use
	 */
	String name();
	
	
	/**
	 * Sort Order.<br/>
	 * The order to sort the column in the index
	 *
	 * @return the sort order
	 */
	SortOrder sortOrder() default SortOrder.DEFAULT;
	
	/**
	 * Null order.<br/>
	 * The sort order for nulls in the index
	 *
	 * @return the null order
	 */
	NullOrder nullOrder() default NullOrder.DEFAULT;
	
	/**
	 * Column order.<br/>
	 * The order the columns will be applied if this index will have multiple columns. 
	 *
	 * @return the int
	 */
	int columnOrder() default 1;
	
}
