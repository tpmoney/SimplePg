package com.ncitguys.db.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Annotation Sequence.<br/>
 * This indicates that a column field in a database table uses a specified sequence 
 * to generate a value if no value is provided
 * 
 * @author Tevis Money
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Sequence {
	
	/**
	 * Name.<br/>
	 * The name of the sequence in the database. 
	 *
	 * @return the name of the sequence to use
	 */
	String name();
	
	
	/**
	 * Min value.<br/>
	 * The minimum value of this sequence. The default is 1
	 *
	 * @return the minimum sequence value
	 */
	int minValue() default 1;
	
	/**
	 * Start value.<br/>
	 * The starting value for this sequence. The default is 1
	 *
	 * @return the start value for this sequence
	 */
	long startValue() default 1;
	
	/**
	 * Increment.<br/>
	 * The value to increment the sequence by. The default is 1
	 *
	 * @return the value to increment the sequence by
	 */
	int increment() default 1;
	
}
