package com.ncitguys.db.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The Interface Indexes.<br/>
 * A container annotation for the Index annotation. This allows a column
 * to be declared to appear in multiple indexes.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Indexes {
	/**
	 * The array of applied annotations
	 * @return the collection of annotations applied to the column
	 */
	Index[] value();
}
