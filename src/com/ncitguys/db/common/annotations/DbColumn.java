package com.ncitguys.db.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Annotation DbColumn.<br/>
 * This marks a field in the database helpers as a database column field
 * 
 * @author Tevis Money
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DbColumn {
	
	/**
	 * Name.<br/>
	 * The name of this column in the database. If this property is not provided, the name of the field is used.<br/>
	 * If the table class has the {@link DbTable#columnPrefix()} property on it's {@link DbTable} annotation, do not
	 * include the prefix as part of this name.
	 *
	 * @return the alternative column name
	 */
	String name() default "";
	
	/**
	 * Auto increment.<br/>
	 * Indicates whether this column is an auto-incrementing field in the database
	 *
	 * @return true, if this is an auto incrementing field
	 */
	boolean autoIncrement() default false;
	
	/**
	 * Length.<br/>
	 * Indicates the length of this field, if defined. A value less than 
	 * one means no length is defined.
	 *
	 * @return the length of this field
	 */
	int length() default -1;
	
	/**
	 * Scale.<br/>
	 * Indicates the scale of this field, if defined. A value less than
	 * zero means no precision is defined.
	 *
	 * @return the precisions of this field
	 */
	int scale() default -1;
	
	/**
	 * Creation Type.<br/>
	 * What SQL type to use when creating the table, overriding the default determined by field class.
	 * If this is not defined, the SQL type will be determined from the field type. If this is defined, 
	 * it overrides the values defined in {@link #length()} and {@link #scale()}
	 *
	 * @return the string
	 */
	String creationType() default "";
	
	/**
	 * Unique.<br/>
	 * Indicates that this column has a UNIQUE constraint requiring that any given
	 * value appears only once in the table. This is ignored if the column is annotated
	 * with {@link PrimaryKey}
	 * 
	 * @return whether this column has the UNIQUE constraint applied
	 */
	boolean unique() default false;
	
	/**
	 * Allow Nulls.<br/>
	 * Indicates that this column allows null values. If false and a null value 
	 * is encountered, the column will return the JDBC default, which may still
	 * be null.
	 * 
	 * @return whether this column allows null values
	 */
	boolean allowNulls() default true;
}
