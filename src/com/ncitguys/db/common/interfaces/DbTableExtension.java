package com.ncitguys.db.common.interfaces;

import java.lang.reflect.Field;

public interface DbTableExtension {
	
	/**
	 * Gets the creation type for a given field.
	 * Determines what sort of creation type should be used for creating this
	 * table the database for a given field.
	 *
	 * @param f the field
	 * @return the creation type or null if this field should be handled by the default implementation
	 */
	public String getCreationType(Field f);
	
	
}
