package com.ncitguys.db.common;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class DbJoin.<br/>
 * Contains the necessary information to join a given table instance to another table
 * 
 * @author Tevis Money
 */
@Getter
@Setter
public class DbJoin {
	
	/** The SQL join type (JOIN, LEFT JOIN, RIGHT JOIN, OUTER JOIN etc). */
	String joinType = "";
	
	/** The table instance to be joined */
	DbCommon<?> joinInstance;
	
	/** 
	 * The join conditions.<br/>
	 * May optionally start with the SQL keyword "ON"
	 */
	String joinConditions = "";
	
	/**
	 * Instantiates a new db join.
	 *
	 * @param joinType the {@link #joinType join type}
	 * @param joinInstance the {@link #joinInstance join instance}
	 * @param joinConditions the {@link #joinConditions join conditions}
	 */
	public DbJoin(String joinType, DbCommon<?> joinInstance, String joinConditions){
		this.joinType = joinType;
		this.joinInstance = joinInstance;
		this.joinConditions = joinConditions;
	}

}
