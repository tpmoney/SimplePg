package com.ncitguys.db.common;

import com.ncitguys.db.common.annotations.*;
import com.ncitguys.enums.common.IntegerEnum;
import com.ncitguys.enums.common.StringEnum;
import com.ncitguys.helperobjects.PhoneNumber;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The Class DbCommon.<br/>
 * The core class from which all database classes should inherit. Classes which inherit from
 * this calss <b>must</b> implement a nullary constructor.
 *
 * @param <T> the generic type
 * @author Tevis Money
 */
@Getter
@Setter
public abstract class DbCommon<T> implements com.ncitguys.db.common.interfaces.DbTableExtension {

    public static enum Protocol {
        PostgreSQL,
        H2
    }

    public static Protocol PROTOCOL = Protocol.PostgreSQL;

    public static final Logger logger = Logger.getLogger("com.coremgmt.dbcommon");

    /** The suffic to add to the table name in sql queries. */
    private String tableSuffix;

    /** The alias to use in sql queries. */
    private String alias = null;

    /** Flag to indicate that SQL should be printing out before executing. */
    private boolean showSql = false;

    /** The joins to be obtained during an sql query. */
    private LinkedHashMap<String, DbJoin> joins = new LinkedHashMap<String, DbJoin>();

    /** The select columns to obtain during a select query */
    private LinkedHashSet<String> selectColumns = new LinkedHashSet<String>();

    private SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    /** Whether or not this record was selected from database. */
    @Setter(AccessLevel.PRIVATE)
    private boolean selectedFromDatabase = false;

    /**
     * Instantiates a new db common.
     */
    public DbCommon() {
        this(null);
    }

    /**
     * Instantiates a new db common with an alias.
     *
     * @param alias the alias
     */
    public DbCommon(String alias) {
        setAlias(alias);
    }

    /**
     * Gets the name of this table in the database.
     *
     * @return the table name
     */
    public String getTableName() {
        String schema = this.getClass().getAnnotation(DbTable.class).schema();
        if (!schema.isEmpty()) {
            schema = schema + ".";
        }
        return schema + this.getClass().getAnnotation(DbTable.class).name() + (tableSuffix == null ? "" : tableSuffix);
    }

    /**
     * Resets this object for new queries. In particular, resets the alias, joins and select columns.
     */
    public void reset() {
        alias = null;
        joins.clear();
        selectColumns.clear();
    }

    public ArrayList<Field> getDeclaredFields(Class<?> clazz) {
        ArrayList<Field> fieldList = new ArrayList<Field>(Arrays.asList(clazz.getDeclaredFields()));
        if (clazz.getSuperclass() != null) {
            fieldList.addAll(getDeclaredFields(clazz.getSuperclass()));
        }
        return fieldList;
    }

    /**
     * Checks if this instance refers to the same record in the database as another instance.<br/>
     * The default implementation of this compares all the primary key fields between the instances
     *
     * @param otherRecord the other record
     * @return true, if the instances refer to the same record
     */
    public boolean isSameRecord(T otherRecord) {
        boolean same = true;
        for (Field f : getDeclaredFields(this.getClass())) {
            try {
                if (f.isAnnotationPresent(PrimaryKey.class)) {
                    f.setAccessible(true);
                    Object thisValue = f.get(this);
                    Object otherValue = f.get(otherRecord);
                    if (thisValue == null || otherValue == null) {
                        same = false;
                        break;
                    }
                    if (!thisValue.equals(otherValue)) {
                        same = false;
                        break;
                    }
                }
            } catch (IllegalAccessException e) {
                same = false;
                break;
            }
        }
        return same;
    }

    /**
     * Adds a join to this table for sql queries.
     *
     * @param joinObject the join object
     */
    public void addJoin(DbJoin joinObject) {
        joins.put(joinObject.getJoinInstance().getAlias(), joinObject);
    }

    /**
     * Gets a join by name.
     *
     * @param joinName  the join name
     * @param castClazz the class to cast the join to
     * @return the join
     */
    public <U> U getJoin(String joinName, Class<U> castClazz) {
        DbJoin join = joins.get(joinName);
        if (join != null) {
            Object returnObject = join.getJoinInstance();
            if (castClazz.isAssignableFrom(returnObject.getClass())) {
                return castClazz.cast(returnObject);
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * Adds a specific column to be selected from the database.<br/>
     * If no columns are specified or "*" is the only column specified, all columns will be selected
     *
     * @param columnName the column name to select
     */
    public void addSelectColumn(String columnName) {
        selectColumns.add(columnName);
    }

    /**
     * Sets the alias for this table, removing any "." characters.
     *
     * @param alias the new alias
     */
    public void setAlias(String alias) {
        if (alias != null) {
            this.alias = alias.replace(".", "").trim();
        } else {
            alias = null;
        }
    }

    /**
     * Gets a column name aliased with this table's alias, or the plain name if no alias is specified.</br>
     * This <b>does not</b> prepend the column prefix to the column name if one is specified
     *
     * @param columnName the column name
     * @return the aliased column name
     * @see {@link DbTable#columnPrefix()}
     */
    public String getAs(String columnName) {
        if (alias != null && !alias.isEmpty()) {
            return getAlias() + "." + columnName;
        }
        return columnName;
    }

    /**
     * Gets a column name aliased with this table's alias, or the plain name if no alias is specified.</br>
     * This <b>does</b> prepend the column prefix to the column name if one is specified
     *
     * @param columnName the column name
     * @return the aliased and prefixed column name
     * @see {@link DbTable#columnPrefix()}
     */
    public String getAsPrefixed(String columnName) {
        String prefix = this.getClass().getAnnotation(DbTable.class).columnPrefix();
        String alias = "";
        if (this.alias != null && !this.alias.isEmpty()) {
            alias = getAlias() + ".";
        }
        return alias + prefix + columnName;
    }

    /**
     * Gets column name aliased with this table's alias or the plain name if no alias is specified
     * from a provided field.
     *
     * @param field the field
     * @return the aliased column name
     */
    private String getAliasedColumnName(Field field) {
        return getAs(getUnaliasedColumnName(field));
    }

    /**
     * Gets the column name from a field unaliased.
     *
     * @param field the field
     * @return the unaliased column name
     */
    private String getUnaliasedColumnName(Field field) {
        String prefix = this.getClass().getAnnotation(DbTable.class).columnPrefix();
        if (field.getAnnotation(DbColumn.class).name().isEmpty()) {
            return prefix + field.getName();
        } else {
            return prefix + field.getAnnotation(DbColumn.class).name();
        }
    }


    @Override
    public String toString() {
        StringBuilder descr = new StringBuilder(this.getClass().getName());
        descr.append(" [");
        for (Field field : getDeclaredFields(this.getClass())) {
            if (field.isAnnotationPresent(DbColumn.class)) {
                field.setAccessible(true);
                descr.append(field.getName()).append(": ");
                try {
                    Object value = field.get(this);
                    if (value == null) {
                        descr.append("NULL, ");
                    } else {
                        descr.append(value.toString()).append(", ");
                    }
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    descr.append("ERRROR READING FIELD, ");
                }

            }
        }
        descr.setLength(descr.length() - 2);
        descr.append(" ]");

        return descr.toString();
    }

    /**
     * Loads this instance with the values from the current row in a given result set.
     *
     * @param res the res
     */
    public void load(ResultSet res) {
        List<Field> fields = getDeclaredFields(this.getClass());
        for (Field field : fields) {
            if (field.isAnnotationPresent(DbColumn.class)) {
                field.setAccessible(true);
                boolean converted = false;
                try {
                    Class<?> fieldType = field.getType();
                    String fieldName = getAliasedColumnName(field);

                    if (Integer.class.equals(fieldType)) {
                        field.set(this, res.getInt(fieldName));
                        converted = true;
                    } else if (Double.class.equals(fieldType)) {
                        field.set(this, res.getDouble(fieldName));
                        converted = true;
                    } else if (Long.class.equals(fieldType)) {
                        field.set(this, res.getLong(fieldName));
                        converted = true;
                    } else if (BigDecimal.class.equals(fieldType)) {
                        field.set(this, res.getBigDecimal(fieldName));
                        converted = true;
                    } else if (Boolean.class.equals(fieldType)) {
                        field.set(this, res.getBoolean(fieldName));
                        converted = true;
                    } else if (String.class.equals(fieldType)) {
                        field.set(this, res.getString(fieldName));
                        converted = true;
                    } else if (Date.class.equals(fieldType)) {
                        field.set(this, res.getTimestamp(fieldName));
                        converted = true;
                    } else if (Timestamp.class.equals(fieldType)) {
                        field.set(this, res.getTimestamp(fieldName));
                        converted = true;
                    } else if (GregorianCalendar.class.equals(fieldType)) {
                        Timestamp ts = res.getTimestamp(fieldName);
                        if (ts == null) {
                            field.set(this, null);
                        } else {
                            GregorianCalendar gc = new GregorianCalendar();
                            gc.setTime(ts);
                            field.set(this, gc);
                        }
                        converted = true;
                    } else if (IntegerEnum.class.isAssignableFrom(fieldType)) {
                        try {
                            Method m = fieldType.getMethod("create", Integer.class);
                            field.set(this, m.invoke(null, res.getInt(fieldName)));
                            converted = true;
                        } catch (NoSuchMethodException | InvocationTargetException e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                        }
                    } else if (StringEnum.class.isAssignableFrom(fieldType)) {
                        try {
                            Method m = fieldType.getMethod("create", String.class);
                            field.set(this, m.invoke(null, res.getString(fieldName)));
                            converted = true;
                        } catch (NoSuchMethodException | InvocationTargetException e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                        }
                    } else if (PhoneNumber.class.equals(fieldType)) {
                        field.set(this, PhoneNumber.parse(res.getString(fieldName)));
                        converted = true;
                    } else {
                        field.set(this, res.getObject(fieldName));
                    }

                    if (field.getAnnotation(DbColumn.class).allowNulls()) {
                        if (res.wasNull()) {
                            field.set(this, null);
                        }
                    }

                } catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
                    if (e.getMessage().contains("The column name") && e.getMessage().contains("was not found in this ResultSet")) {
                        converted = true;
                    } else {
                        logger.log(Level.WARNING, e.getMessage(), e);
                    }
                } finally {
                    if (!converted) {
                        logger.log(Level.WARNING, "Unable to properly convert value for field " + field.getName()
                                + " of type " + field.getType().getCanonicalName() + " in class " + this.getClass().getCanonicalName());
                    }
                }
            }
        }
        this.setSelectedFromDatabase(true);
    }


    /**
     * Selects a single instance from the database.
     *
     * @param conn        the database connection to use
     * @param whereClause the where clause to use
     * @param params      the parameters to insert into the query
     * @return an instance of this class with the results of the query
     */
    public T selectSingle(Connection conn, String whereClause, Object... params) {

        ArrayList<T> res = selectMultiple(conn, whereClause, params);
        if (!res.isEmpty()) {
            return res.get(0);
        }
        return null;
    }

    /**
     * Sets the parameters for a query from a given collection of parameters.
     *
     * @param query          the query
     * @param collection     the collection of parameters
     * @param parameterIndex the current parameter index
     * @return the next parameter index
     * @throws SQLException if an error occurs setting a parameter
     */
    private int setParameters(PreparedStatement query, Object[] collection, int parameterIndex) throws SQLException {
        List<Object> list = Arrays.asList(collection);
        return setParameters(query, list, parameterIndex);
    }

    /**
     * Sets the parameters for a query from a given collection of parameters.
     *
     * @param query          the query
     * @param collection     the collection of parameters
     * @param parameterIndex the current parameter index
     * @return the next parameter index
     * @throws SQLException if an error occurs setting a parameter
     */
    private int setParameters(PreparedStatement query, Collection<?> collection, int parameterIndex) throws SQLException {
        int index = parameterIndex;
        for (Object obj : collection) {
            if (obj == null) {
                query.setNull(index, Types.NULL);
                index++;
            } else if (obj instanceof Collection) {
                index = setParameters(query, (Collection<?>) obj, index);
            } else if (obj.getClass().isArray() && obj.getClass().isPrimitive()) {
                index = setParameters(query, (Object[]) obj, index);
            } else {
                Class<?> parameterType = obj.getClass();
                if (Integer.class.equals(parameterType) || int.class.equals(parameterType)) {
                    query.setInt(index, ((Integer) obj).intValue());
                } else if (Double.class.equals(parameterType) || double.class.equals(parameterType)) {
                    query.setDouble(index, ((Double) obj).doubleValue());
                } else if (Long.class.equals(parameterType) || long.class.equals(parameterType)) {
                    query.setLong(index, ((Long) obj).longValue());
                } else if (BigDecimal.class.equals(parameterType)) {
                    query.setBigDecimal(index, (BigDecimal) obj);
                } else if (Boolean.class.equals(parameterType) || boolean.class.equals(parameterType)) {
                    query.setBoolean(index, ((Boolean) obj).booleanValue());
                } else if (String.class.equals(parameterType)) {
                    query.setString(index, (String) obj);
                } else if (Date.class.equals(parameterType)) {
                    Timestamp t = new Timestamp(((Date) obj).getTime());
                    query.setTimestamp(index, t);
                } else if (GregorianCalendar.class.equals(parameterType)) {
                    Timestamp t = new Timestamp(((GregorianCalendar) obj).getTimeInMillis());
                    query.setTimestamp(index, t);
                } else if (IntegerEnum.class.isAssignableFrom(parameterType)) {
                    query.setInt(index, ((IntegerEnum) obj).getDbValue().intValue());
                } else if (StringEnum.class.isAssignableFrom(parameterType)) {
                    query.setString(index, ((StringEnum) obj).getDbValue());
                } else if (PhoneNumber.class.equals(parameterType)) {
                    query.setString(index, ((PhoneNumber) obj).toString());
                } else {
                    //System.out.println("Setting plain object!");
                    query.setObject(index, obj);
                }
                index++;
            }
        }
        return index;
    }

    /**
     * Selects multiple instances from the database.
     *
     * @param conn        the database connection to use
     * @param whereClause the where clause to use
     * @param params      the parameters to set in the query
     * @return a list of instances matching the query
     */
    public ArrayList<T> selectMultiple(Connection conn, String whereClause, Object... params) {
        return selectMultiple(conn, whereClause, params, null, null, null);
    }

    public ArrayList<T> selectMultiple(Connection conn, String whereClause, Object[] params, String orderByClause, Integer limit) {
        return selectMultiple(conn, whereClause, params, orderByClause, limit, null);
    }

    /**
     * Selects multiple instances from the database.
     *
     * @param conn          the database connection to use
     * @param whereClause   the where clause to use
     * @param params        the parameters to set in the query
     * @param orderByClause the order by clause to use
     * @param limit         a limit to the number of results to be returned
     * @param offset        the offset to apply to the query
     * @return a list of instances matching the query
     */
    @SuppressWarnings("unchecked")
    public ArrayList<T> selectMultiple(Connection conn, String whereClause, Object[] params, String orderByClause, Integer limit, Integer offset) {
        PreparedStatement query = null;
        QueryResults res = null;
        ArrayList<T> resultList = new ArrayList<T>();
        try {

            StringBuilder queryString = new StringBuilder("SELECT");
            appendSelectColumns(queryString, this);
            queryString.setLength(queryString.length() - 1);

            queryString.append(" FROM ")
                    .append(getTableName());
            if (getAlias() != null && !getAlias().isEmpty()) {
                queryString.append(" AS \"")
                        .append(getAlias())
                        .append("\"");
            }
            for (DbJoin join : getJoins().values()) {
                appendJoins(queryString, join);
            }

            if (whereClause != null) {
                queryString.append(" ");
                whereClause = whereClause.trim();
                if (!whereClause.toUpperCase().startsWith("WHERE")) {
                    queryString.append("WHERE ");
                }
                queryString.append(whereClause);
            }

            if (orderByClause != null) {
                queryString.append(" ");
                orderByClause = orderByClause.trim();
                if (!orderByClause.toUpperCase().startsWith("ORDER BY")) {
                    queryString.append("ORDER BY ");
                }
                queryString.append(orderByClause);
            }

            if (limit != null && limit.compareTo(0) == 1) {
                queryString.append(" LIMIT ").append(limit);
            }

            if (offset != null) {
                queryString.append(" OFFSET ").append(offset);
            }

            res = executeQuery(conn, queryString.toString(), params);

            while (res.getResults().next()) {
                T returnObject = (T) instantiateAndLoad(this, res);
                resultList.add(returnObject);
            }
        } catch (SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException |
                SecurityException | IllegalArgumentException | InvocationTargetException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        } finally {
            if (res != null) {
                try {
                    res.close();
                } catch (Exception e) {
                }
            }

            if (query != null) {
                try {
                    if (!query.isClosed()) {
                        query.close();
                    }
                } catch (Exception e) {
                }
            }
        }

        return resultList;
    }

    @SuppressWarnings("unchecked")
    /**
     * Loads an instance of a given database table from a query result
     * @param instance the instance used to generate the query
     * @param res the query result
     * @return a new instance with the results of the query loaded
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    protected DbCommon<?> instantiateAndLoad(DbCommon<?> instance, QueryResults res) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        @SuppressWarnings("rawtypes")
        Constructor<? extends DbCommon> constructor = instance.getClass().getDeclaredConstructor();
        constructor.setAccessible(true);
        DbCommon<T> returnObject = constructor.newInstance(new Object[]{});
        returnObject.setAlias(instance.getAlias());
        returnObject.setTableSuffix(instance.getTableSuffix());
        returnObject.load(res.getResults());
        for (DbJoin join : instance.getJoins().values()) {
            DbCommon<?> joinObject = instantiateAndLoad(join.getJoinInstance(), res);
            DbJoin returnJoin = new DbJoin(join.getJoinType(), joinObject, join.getJoinConditions());
            returnObject.addJoin(returnJoin);
        }

        return returnObject;
    }

    /**
     * Save.<br/>
     * A convenience method which either updates or inserts this instance in
     * the database depending on the value of {@link #isSelectedFromDatabase()}.
     * If the value is true, the {@link #update(Connection)} method is called, otherwise
     * the {@link #insert(Connection)} method is called.
     *
     * @param conn the database connection to use
     * @return true, if successful with the result of the insert or update loaded into this instance
     */
    public boolean save(Connection conn) {
        if (isSelectedFromDatabase()) {
            return update(conn);
        } else {
            return insert(conn);
        }
    }

    /**
     * Updates this instance in the database by the primary key fields.<br/>
     * This requires having at least one field marked with
     * the {@link com.ncitguys.db.common.annotations.PrimaryKey PrimaryKey annotation}
     * and does not update any fields so marked.
     *
     * @param conn the database connection to use
     * @return true, if successful. If the {@link #PROTOCOL} is {@link Protocol#PostgreSQL PostgeSQL} the result of the update will be loaded into this instance
     */
    public boolean update(Connection conn) {

        try {
            StringBuilder queryString = new StringBuilder("UPDATE ");
            queryString.append(getTableName()).append(" SET (");

            //Store the fields in order to ensure proper update order, capture primary key fiels
            LinkedList<Field> updateFields = new LinkedList<Field>();
            LinkedList<Field> primaryKeys = new LinkedList<Field>();
            for (Field f : getDeclaredFields(this.getClass())) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(DbColumn.class) && !f.isAnnotationPresent(PrimaryKey.class)) {
                    updateFields.add(f);
                    queryString.append(getUnaliasedColumnName(f)).append(", ");
                } else if (f.isAnnotationPresent(PrimaryKey.class)) {
                    primaryKeys.add(f);
                }
            }
            if (primaryKeys.isEmpty()) {
                throw new SQLException("No primary keys defined for database table class " + this.getClass().getCanonicalName());
            }
            queryString.setLength(queryString.length() - 2);
            queryString.append(") = (");

            //Add a parameter for each field and add the field value to an ordered parameter list
            LinkedList<Object> params = new LinkedList<Object>();
            for (Field f : updateFields) {
                queryString.append("?,");
                params.add(f.get(this));
            }
            queryString.setLength(queryString.length() - 1);
            queryString.append(")  WHERE ");

            for (Field f : primaryKeys) {
                queryString.append(getUnaliasedColumnName(f)).append(" = ? AND ");
                params.add(f.get(this));
            }
            queryString.setLength(queryString.length() - 4);

            switch (PROTOCOL) {
                case PostgreSQL:
                    queryString.append(" RETURNING *");
                    break;

                case H2:
                    //H2 does not support the RETURNING statement
                    break;

            }

            boolean returnValue = false;

            try (QueryResults res = executeQuery(conn, queryString.toString(), params.toArray())) {

                switch (PROTOCOL) {
                    case PostgreSQL:
                        if (res.getResults().next()) {
                            String alias = getAlias(); //Preserve the current alias before getting columns from result set
                            setAlias(null); //Clear the alias for the load
                            load(res.getResults());
                            setAlias(alias);
                            returnValue = true;
                        }
                        break;

                    case H2:
                        //Since H2 doesn't support RETURNING, just return true
                        // as long as no exceptions were thrown
                        returnValue = true;
                        break;

                }


            } catch (SQLException e) {
                throw e;
            }

            return returnValue;
        } catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
            logger.log(Level.WARNING, e.getMessage(), e);

        }

        return false;
    }

    /**
     * Inserts this instance into the database without starting a new transaction or savepoint.
     *
     * @param conn the database connection to use
     * @return true, if successful with the result of the insert loaded into this instance
     */
    public boolean insert(Connection conn) {
        return insert(conn, false);
    }

    /**
     * Inserts this instance into the database.<br/>
     * <br/>
     * If when calling you are expecting the possibility of an error (for example, a unique constraint violation) that
     * you will be subsequently handling, it is advised to set withTransaction to true. This prevents the secenario
     * where a SQL exception being thrown during the insert causes the entire transaction to either need to be committed
     * or abandoned in order to continue processing.
     *
     * @param conn            the database connection to use
     * @param withTransaction if true, this will start a new transaction or savepoint before performing the insert
     *                        and commit or roll it back at the end.
     * @return true, if successful. If the {@link #PROTOCOL} is {@link Protocol#PostgreSQL PostgeSQL} the result of the insert will be loaded into this instance
     */
    public boolean insert(Connection conn, boolean withTransaction) {
        try {
            StringBuilder queryString = new StringBuilder("INSERT INTO ");
            queryString.append(getTableName()).append(" (");

            //Store the fields in order to ensure proper insert order
            LinkedList<Field> insertFields = new LinkedList<Field>();
            for (Field f : getDeclaredFields(this.getClass())) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(DbColumn.class)) {
                    if (!f.getAnnotation(DbColumn.class).autoIncrement() //Not an auto increment field
                            || (f.getAnnotation(DbColumn.class).autoIncrement() && f.get(this) != null) //Auto increment field and we've supplied a value
                            || (f.isAnnotationPresent(Sequence.class) && f.get(this) == null)) {
                        insertFields.add(f);
                        queryString.append(getUnaliasedColumnName(f)).append(", ");
                    }
                }
            }
            queryString.setLength(queryString.length() - 2);
            queryString.append(") VALUES (");

            //Add a parameter for each field and add the field value to an ordered parameter list
            LinkedList<Object> params = new LinkedList<Object>();
            for (Field f : insertFields) {
                if (f.isAnnotationPresent(Sequence.class) && f.get(this) == null) {
                    queryString.append("nextval('" + f.getAnnotation(Sequence.class).name() + "'),");
                } else {
                    queryString.append("?,");
                    params.add(f.get(this));
                }
            }
            queryString.setLength(queryString.length() - 1);
            queryString.append(")");

            switch (PROTOCOL) {
                case PostgreSQL:
                    queryString.append(" RETURNING *");
                    break;

                case H2:
                    //H2 does not support the RETURNING statement
                    break;
            }


            boolean returnValue = false;

            try (QueryResults res = executeQuery(conn, queryString.toString(), params.toArray(), withTransaction)) {

                switch (PROTOCOL) {
                    case PostgreSQL:
                        if (res.getResults().next()) {
                            String alias = getAlias(); //Preserve the current alias before getting columns from result set
                            setAlias(null); //Clear the alias for the load
                            load(res.getResults());
                            setAlias(alias);
                            returnValue = true;
                        }
                        break;

                    case H2:
                        //Since H2 doesn't support RETURNING, just return true if no exceptions were thrown
                        returnValue = true;
                        break;
                }

            } catch (SQLException e) {
                throw e;
            }

            return returnValue;

        } catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
            logger.log(Level.WARNING, e.getMessage(), e);

        }

        return false;
    }


    /**
     * Deletes this instance from the database.
     *
     * @param conn the database connection to use
     * @return the deleted object
     */
    public T delete(Connection conn) {

        try {
            StringBuilder whereClause = new StringBuilder("WHERE ");
            ArrayList<Object> params = new ArrayList<Object>();
            for (Field f : getDeclaredFields(this.getClass())) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(DbColumn.class) && f.isAnnotationPresent(PrimaryKey.class)) {
                    whereClause.append(getUnaliasedColumnName(f)).append(" = ? AND ");
                    params.add(f.get(this));
                }
            }
            whereClause.setLength(whereClause.length() - 4);


            return deleteSingle(conn, whereClause.toString(), params);

        } catch (IllegalAccessException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return null;
    }

    public T deleteSingle(Connection conn, String whereClause, Object... params) {
        ArrayList<T> results = delete(conn, whereClause, params);
        if (results.isEmpty()) {
            return null;
        } else {
            return results.get(0);
        }
    }

    @SuppressWarnings("unchecked")
    public ArrayList<T> delete(Connection conn, String whereClause, Object... params) {
        ArrayList<T> resultList = new ArrayList<T>();
        try {

            StringBuilder queryString = new StringBuilder("DELETE FROM ");
            queryString.append(getTableName());

            if (whereClause != null) {
                queryString.append(" ");
                whereClause = whereClause.trim();
                if (!whereClause.toUpperCase().startsWith("WHERE")) {
                    queryString.append("WHERE ");
                }
                queryString.append(whereClause);
            }

            switch (PROTOCOL) {
                case PostgreSQL:
                    queryString.append(" RETURNING *");
                    break;

                case H2:
                    //H2 does not support the RETURNING statement
                    break;
            }


            try (QueryResults res = executeQuery(conn, queryString.toString(), params)) {

                switch (PROTOCOL) {
                    case PostgreSQL:
                        while (res.getResults().next()) {
                            @SuppressWarnings("rawtypes")
                            Constructor<? extends DbCommon> constructor = this.getClass().getDeclaredConstructor();
                            constructor.setAccessible(true);
                            DbCommon<T> returnObject = constructor.newInstance(new Object[]{});
                            returnObject.setAlias(getAlias());
                            returnObject.setTableSuffix(getTableSuffix());
                            returnObject.load(res.getResults());
                            resultList.add((T) returnObject);
                        }
                        break;

                    case H2:
                        //Return nothing in the result list because H2 doesn't support RETURNING
                        break;
                }

            } catch (SQLException e) {
                throw e;
            }
        } catch (SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException |
                SecurityException | IllegalArgumentException | InvocationTargetException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return resultList;
    }

    @SuppressWarnings("unchecked")
    public Integer count(Connection conn, String whereClause, Object... params) {
        try {

            StringBuilder queryString = new StringBuilder("SELECT COUNT(*) ");

            queryString.append(" FROM ")
                    .append(getTableName());
            if (getAlias() != null && !getAlias().isEmpty()) {
                queryString.append(" AS \"")
                        .append(getAlias())
                        .append("\"");
            }
            for (DbJoin join : getJoins().values()) {
                appendJoins(queryString, join);
            }

            if (whereClause != null) {
                queryString.append(" ");
                whereClause = whereClause.trim();
                if (!whereClause.toUpperCase().startsWith("WHERE")) {
                    queryString.append("WHERE ");
                }
                queryString.append(whereClause);
            }


            try (QueryResults res = executeQuery(conn, queryString.toString(), params)) {

                if (res.getResults().next()) {
                    return res.getResults().getInt(1);
                }

            } catch (SQLException e) {
                throw e;
            }
        } catch (SQLException | SecurityException | IllegalArgumentException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return 0;
    }

    public boolean dropTable(Connection conn, boolean cascade) {
        StringBuilder query = new StringBuilder("DROP TABLE ").append(getTableName());
        if (cascade) {
            query.append(" CASCADE");
        }

        try (PreparedStatement ps = conn.prepareStatement(query.toString())) {
            ps.execute();
            return true;
        } catch (SQLException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
        return false;
    }

    /**
     * Creates the table represented by this class in the database.<br/>
     * If the sequences defined on this table's columns already exist, this will produce an error.
     * If the indexes defined for this table's columns already exist, this will produce an error.
     *
     * @param conn the database connection to use
     * @return true, if successful
     */
    public boolean createTable(Connection conn) {
        return createTable(conn, true, false, true, false);
    }

    /**
     * Creates the table represented by this class in the database<br/>.
     *
     * @param conn            the database connection to use
     * @param createSequences if true, this will create (but not replace) sequences defined by the table
     * @param createIndexes   if true, this will create (but not replace) indexes defined on the columns
     * @return true, if successful
     */
    public boolean createTable(Connection conn, boolean createSequences, boolean createIndexes) {
        return createTable(conn, createSequences, false, createIndexes, false);
    }


    /**
     * Creates the table represented by this class in the database.
     *
     * @param conn             the database connection to use
     * @param createSequences  if true, this will create sequences defined by the table
     * @param replaceSequences if true, this will replace the any sequences defined by the table that already exist
     * @param createIndexes    if true, this will create the indexes defined on the table columns
     * @param replaceIndexes   if true, this will replace any indexes defined on the table columns the already exist
     * @return true, if successful
     */
    public boolean createTable(Connection conn, boolean createSequences, boolean replaceSequences, boolean createIndexes, boolean replaceIndexes) {

        //Whether or not to re-enable auto commit at the end of the transaction.
        // If the user is already managing the transaction states, we don't want to disrupt that
        boolean enableAutoCommit = false;
        Savepoint s = null;
        try {

            if (conn.getAutoCommit()) {
                conn.setAutoCommit(false);
                enableAutoCommit = true;
            }
            s = conn.setSavepoint();

            try {
                StringBuilder queryString = new StringBuilder("CREATE TABLE ");
                queryString.append(getTableName()).append(" (");

                LinkedList<Field> sequences = new LinkedList<>();
                LinkedList<Field> primaryKeys = new LinkedList<>();


                for (Field f : getDeclaredFields(this.getClass())) {

                    //Save the fields that need sequences
                    if (createSequences && f.isAnnotationPresent(Sequence.class)) {
                        sequences.add(f);
                        createSequence(conn, f.getAnnotation(Sequence.class), replaceSequences);
                    }

                    if (f.isAnnotationPresent(PrimaryKey.class)) {
                        primaryKeys.add(f);
                    }

                    if (f.isAnnotationPresent(DbColumn.class)) {
                        f.setAccessible(true);
                        queryString.append(getFieldCreationString(f)).append(", ");
                    }
                }

                queryString.append("PRIMARY KEY (");
                for (Field f : primaryKeys) {
                    queryString.append(getUnaliasedColumnName(f)).append(", ");
                }

                queryString.setLength(queryString.length() - 2);

                queryString.append(")");

                queryString.append(");");

                PreparedStatement ps = conn.prepareStatement(queryString.toString());
                ps.execute();


                try {
                    ps.close();
                } catch (Exception e) {/*Do Nothing*/}

                if (createSequences) {
                    for (Field f : sequences) {
                        setSequenceOwnership(conn, f);
                    }
                }

                if (createIndexes) {
                    if (!createIndexes(conn, replaceIndexes)) {
                        throw new SQLException("Error creating table indexes");
                    }
                }

                conn.commit();
                return true;
            } catch (SQLException e) {
                conn.rollback(s);
                throw e;
            }
        } catch (SQLException | IllegalArgumentException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        } finally {

            try {
                if (enableAutoCommit) {
                    conn.setAutoCommit(true);
                }
            } catch (SQLException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }

        return false;
    }

    public boolean createIndexes(Connection conn, boolean replaceIfExists) throws SQLException {

        boolean enableAutoCommit = false;
        Savepoint s = null;
        try {

            if (conn.getAutoCommit()) {
                conn.setAutoCommit(false);
                enableAutoCommit = true;
            }
            s = conn.setSavepoint();

            try {
                HashMap<String, LinkedList<Field>> indexes = new HashMap<>();

                for (Field f : getDeclaredFields(this.getClass())) {

                    //Save the fields that need indexing
                    if (f.isAnnotationPresent(Index.class) || f.isAnnotationPresent(Indexes.class)) {
                        //Handle a single index on a field
                        if (f.isAnnotationPresent(Index.class)) {
                            String indexName = f.getAnnotation(Index.class).name();
                            LinkedList<Field> fieldList = indexes.get(indexName);
                            if (fieldList == null) {
                                fieldList = new LinkedList<>();
                            }
                            fieldList.add(f);
                            indexes.put(indexName, fieldList);
                        }

                        //Handle multiple indexes n a field
                        if (f.isAnnotationPresent(Indexes.class)) {
                            for (Index i : f.getAnnotation(Indexes.class).value()) {
                                String indexName = i.name();
                                LinkedList<Field> fieldList = indexes.get(indexName);
                                if (fieldList == null) {
                                    fieldList = new LinkedList<>();
                                }
                                fieldList.add(f);
                                indexes.put(indexName, fieldList);
                            }
                        }
                    }
                }

                for (Entry<String, LinkedList<Field>> entry : indexes.entrySet()) {
                    createIndex(conn, entry.getKey(), entry.getValue(), replaceIfExists);
                }

                conn.commit();
                return true;
            } catch (SQLException e) {
                conn.rollback(s);
                throw e;
            }
        } catch (SQLException | IllegalArgumentException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        } finally {

            try {
                if (enableAutoCommit) {
                    conn.setAutoCommit(true);
                }
            } catch (SQLException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }

        return false;
    }

    private void createIndex(Connection conn, final String indexName, LinkedList<Field> fieldList, boolean replaceIfExists) throws SQLException {

        String fullIndexName = indexName;
        if (getTableSuffix() != null) {
            fullIndexName = fullIndexName + getTableSuffix();
        }

        if (replaceIfExists) {

            try (PreparedStatement dropS = conn.prepareStatement("DROP INDEX IF EXISTS " + fullIndexName)) {
                dropS.execute();
            } catch (SQLException e) {
                throw e;
            }
        }

        StringBuilder query = new StringBuilder("CREATE INDEX ").append(fullIndexName)
                .append(" ON ").append(getTableName())
                .append("( ");

        Collections.sort(fieldList, new Comparator<Field>() {

            @Override
            public int compare(Field o1, Field o2) {
                int o1Order = 0;
                int o2Order = 0;

                if (o1.isAnnotationPresent(Index.class) && o1.getAnnotation(Index.class).name().equalsIgnoreCase(indexName)) {
                    o1Order = o1.getAnnotation(Index.class).columnOrder();
                } else {
                    for (Index i : o1.getAnnotation(Indexes.class).value()) {
                        if (i.name().equalsIgnoreCase(indexName)) {
                            o1Order = i.columnOrder();
                            break;
                        }
                    }
                }

                if (o2.isAnnotationPresent(Index.class) && o2.getAnnotation(Index.class).name().equalsIgnoreCase(indexName)) {
                    o2Order = o2.getAnnotation(Index.class).columnOrder();
                } else {
                    for (Index i : o2.getAnnotation(Indexes.class).value()) {
                        if (i.name().equalsIgnoreCase(indexName)) {
                            o2Order = i.columnOrder();
                            break;
                        }
                    }
                }
                return Integer.compare(o1Order, o2Order);
            }

        });
        for (Field f : fieldList) {
            Index i = null;

            if (f.isAnnotationPresent(Index.class) && f.getAnnotation(Index.class).name().equalsIgnoreCase(indexName)) {
                i = f.getAnnotation(Index.class);
            } else {
                for (Index tempi : f.getAnnotation(Indexes.class).value()) {
                    if (tempi.name().equalsIgnoreCase(indexName)) {
                        i = tempi;
                        break;
                    }
                }
            }
            query.append(getUnaliasedColumnName(f)).append(i.sortOrder().getSqlValue()).append(i.nullOrder().getSqlValue()).append(", ");
        }
        query.setLength(query.length() - 2);
        query.append(")");

        try (PreparedStatement ps = conn.prepareStatement(query.toString())) {
            ps.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    private void setSequenceOwnership(Connection conn, Field f) throws SQLException {
        String sequenceName = f.getAnnotation(Sequence.class).name();
        if (!this.getClass().getAnnotation(DbTable.class).schema().isEmpty()) {
            sequenceName = this.getClass().getAnnotation(DbTable.class).schema() + "." + sequenceName;
        }

        try (PreparedStatement query = conn.prepareStatement("ALTER SEQUENCE " + sequenceName + " OWNED BY "
                + getTableName() + "." + getUnaliasedColumnName(f))) {
            query.execute();
        }
    }

    private void createSequence(Connection conn, Sequence s, boolean replaceIfExists) throws SQLException {

        String sequenceName = s.name();
        if (!this.getClass().getAnnotation(DbTable.class).schema().isEmpty()) {
            sequenceName = this.getClass().getAnnotation(DbTable.class).schema() + "." + sequenceName;
        }

        if (replaceIfExists) {
            try (PreparedStatement exists = conn.prepareStatement("SELECT * FROM information_schema.sequences "
                    + "WHERE sequence_name = ? and sequence_schema ILIKE ?")) {
                exists.setString(1, s.name());
                if (!this.getClass().getAnnotation(DbTable.class).schema().isEmpty()) {
                    exists.setString(2, this.getClass().getAnnotation(DbTable.class).schema());
                } else {
                    exists.setString(2, "%");
                }

                try (ResultSet exRes = exists.executeQuery()) {
                    if (exRes.next()) {
                        try (PreparedStatement dropS = conn.prepareStatement("DROP SEQUENCE " + sequenceName)) {
                            dropS.execute();
                        }
                    }
                }
            } catch (SQLException e) {
                throw e;
            }
        }

        String query = "CREATE SEQUENCE " + sequenceName
                + " INCREMENT BY " + s.increment()
                + " MINVALUE " + s.minValue()
                + " START WITH " + s.startValue();

        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    private String getFieldCreationString(Field f) {
        StringBuilder sb = new StringBuilder(getUnaliasedColumnName(f));
        sb.append(" ");

        String type = f.getAnnotation(DbColumn.class).creationType();
        if (type != null && !type.isEmpty()) {
            //Use the overridden type defined on the annotation
            sb.append(type);
        } else {
            //If no override type, call the extension method
            type = getCreationType(f);
            if (type != null && !type.isEmpty()) {
                //Use the type returned by the extension method
                sb.append(type);
            } else {
                //If no extension type, use the defaults
                Class<?> fieldType = f.getType();
                if (Integer.class.equals(fieldType) || int.class.equals(fieldType)) {
                    if (f.getAnnotation(DbColumn.class).autoIncrement() && !f.isAnnotationPresent(Sequence.class)) {
                        //Only make a serial if we're not using a defined sequence
                        sb.append("serial");
                    } else {
                        sb.append("integer");
                    }
                } else if (Double.class.equals(fieldType) || double.class.equals(fieldType)) {
                    sb.append("double precision");
                } else if (Long.class.equals(fieldType) || long.class.equals(fieldType)) {
                    if (f.getAnnotation(DbColumn.class).autoIncrement() && !f.isAnnotationPresent(Sequence.class)) {
                        //Only make a serial if we're not using a defined sequence
                        sb.append("bigserial");
                    } else {
                        sb.append("bigint");
                    }
                } else if (BigDecimal.class.equals(fieldType)) {
                    sb.append("numeric");
                    if (f.getAnnotation(DbColumn.class).length() > 0) {
                        sb.append(" (").append(f.getAnnotation(DbColumn.class).length());
                        if (f.getAnnotation(DbColumn.class).scale() >= 0) {
                            sb.append(", ").append(f.getAnnotation(DbColumn.class).scale());
                        }
                        sb.append(")");
                    }
                } else if (Boolean.class.equals(fieldType) || boolean.class.equals(fieldType)) {
                    sb.append("boolean");
                } else if (String.class.equals(fieldType) || StringEnum.class.isAssignableFrom(fieldType) || PhoneNumber.class.equals(fieldType)) {
                    if (f.getAnnotation(DbColumn.class).length() > 0) {
                        sb.append("character varying (").append(f.getAnnotation(DbColumn.class).length()).append(")");
                    } else {
                        sb.append("text");
                    }
                } else if (Date.class.equals(fieldType)) {
                    sb.append("timestamp without time zone");
                } else if (GregorianCalendar.class.equals(fieldType)) {
                    sb.append("timestamp without time zone");
                } else if (IntegerEnum.class.isAssignableFrom(fieldType)) {
                    sb.append("integer");
                } else {
                    sb.append("text");
                }
            }
        }

        if (f.getAnnotation(DbColumn.class).unique() && !f.isAnnotationPresent(PrimaryKey.class)) {
            sb.append(" UNIQUE ");
        }

        if (!f.getAnnotation(DbColumn.class).allowNulls() && !f.isAnnotationPresent(PrimaryKey.class)) {
            sb.append(" NOT NULL ");
        }

        if (f.isAnnotationPresent(Sequence.class)) {
            sb.append(" DEFAULT nextval('" + f.getAnnotation(Sequence.class).name() + "')");
        }

        return sb.toString();
    }


    /* (non-Javadoc)
     * @see com.coremgmt.db.common.interfaces.DbTableExtension#getCreationType(java.lang.reflect.Field)
     */
    public String getCreationType(Field f) {
        /*Do nothing*/
        return null;
    }

    /**
     * Executes a SQL query without a new transaction.
     *
     * @param conn     the database connection to use
     * @param sqlQuery the sql query to execute
     * @param params   the parameters to set on the query
     * @return the result set from the query
     * @throws SQLException if an error occurs setting parameters or selecting from the database
     */
    public QueryResults executeQuery(Connection conn, String sqlQuery, Object[] params) throws SQLException {
        return executeQuery(conn, sqlQuery, params, false);
    }

    /**
     * Executes a SQL query.
     *
     * @param conn            the database connection to use
     * @param sqlQuery        the sql query to execute
     * @param params          the parameters to set on the query
     * @param withTransaction if true, will begin a new transaction or savepoint before executing the query
     * @return the result set from the query
     * @throws SQLException if an error occurs setting parameters or selecting from the database
     */
    public QueryResults executeQuery(Connection conn, String sqlQuery, Object[] params, boolean withTransaction) throws SQLException {

        boolean wasAutoCommit = false; //Whether auto commit was on when we started
        Savepoint sp = null;
        boolean caughtError = false; //Flag to determine how to handle exceptions after the execute
        if (withTransaction) {
            try {
                //Store and disable auto commit
                wasAutoCommit = conn.getAutoCommit();
                conn.setAutoCommit(false);
            } catch (SQLException e) {
                throw new SQLException("Could not disable auto commit as requested", e);
            }

            try {
                //Create the savepoint
                sp = conn.setSavepoint("executesp");
            } catch (SQLException e) {

                String error = "Could not create a savepoint";
                if (wasAutoCommit) {
                    try {
                        conn.setAutoCommit(true);
                    } catch (SQLException e1) {
                        error = error + " and could not re-enable auto commit";
                    }
                }
                throw new SQLException(error, e);
            }
        }

        if (showSql) {
            //Log the raw SQL text and parameters
            StringBuilder sb = new StringBuilder("Query to be executed:\n");
            sb.append(sqlQuery.toString()).append("\n").append(Arrays.toString(params));
            logger.info(sb.toString());
        }

        PreparedStatement query = conn.prepareStatement(sqlQuery);
        if (params != null && params.length > 0) {
            setParameters(query, params, 1);
        }

        if (showSql) {
            //Log the prepared query
            logger.info("Prepared query: " + query.toString());
        }

        QueryResults results = new QueryResults();
        results.setQuery(query);

        try {

            if (query.execute()) {
                results.setResults(query.getResultSet());
            }

            if (withTransaction) {
                conn.commit();
            }

        } catch (SQLException e) {
            if (!showSql) {
                //Log the query and parameters if we aren't in show SQL mode
                StringBuilder sb = new StringBuilder(e.getMessage()).append("\n")
                        .append("The prepared query was: ").append(query.toString()).append("\n")
                        .append("The params were: ").append(Arrays.toString(params)).append("\n")
                        .append("The query was: ").append(sqlQuery.toString());
                logger.warning(sb.toString());
            }

            caughtError = true;
            if (withTransaction) {
                try {
                    //Roll back the local savepoint
                    conn.rollback(sp);
                } catch (SQLException e1) {
                    logger.log(Level.WARNING, "Unable to roll back transaction", e1);
                }
            }

            throw e;
        } finally {
            if (withTransaction && wasAutoCommit) {
                try {
                    //Re-enable auto commit if we turned it off
                    conn.setAutoCommit(true);
                } catch (SQLException e) {
                    if (caughtError) {
                        logger.log(Level.WARNING, "Unable to re-enable autocommit after exception", e);
                    } else {
                        throw new SQLException("Unable to re-enable autocommit", e);
                    }
                }
            }
        }


        return results;
    }

    /**
     * Appends the select columns to a query for a given table instance.
     *
     * @param queryString the query to append to
     * @param table       the table instance to get the selct columns from
     */
    protected void appendSelectColumns(StringBuilder queryString, DbCommon<?> table) {
        boolean all = false;
        if (table.selectColumns.isEmpty() || (table.selectColumns.size() == 1 && table.selectColumns.toArray(new String[]{})[0].equals("*"))) {
            all = true;
        }
        for (Field f : getDeclaredFields(table.getClass())) {
            if (f.isAnnotationPresent(DbColumn.class)) {
                if (all || table.selectColumns.contains(getUnaliasedColumnName(f)) || table.selectColumns.contains(getAliasedColumnName(f))) {
                    queryString.append(" ")
                            .append(table.getAliasedColumnName(f))
                            .append(" AS \"")
                            .append(table.getAliasedColumnName(f))
                            .append("\",");
                }
            }
        }
        for (DbJoin join : table.getJoins().values()) {
            appendSelectColumns(queryString, join.getJoinInstance());
        }
    }

    /**
     * Appends a given join to a query.
     *
     * @param queryString the query to append to
     * @param join        the join to append
     */
    protected void appendJoins(StringBuilder queryString, DbJoin join) {
        queryString.append(" ")
                .append(join.getJoinType().toUpperCase())
                .append(" ")
                .append(join.getJoinInstance().getTableName())
                .append(" AS \"")
                .append(join.getJoinInstance().getAlias())
                .append("\" ");
        String joinConditions = join.getJoinConditions().trim();
        if (!joinConditions.toUpperCase().startsWith("ON ")) {
            queryString.append("ON ");
        }
        queryString.append(joinConditions);

        for (DbJoin subJoin : join.getJoinInstance().getJoins().values()) {
            appendJoins(queryString, subJoin);
        }
    }

    /**
     * Generate a string for specifying an "IN" statement in a SQL query from a collection.<br/>
     * Given a collection of 3 objects, this method will return the string:<br/>
     * " IN ( ?, ?, ? ) " to be used in a "WHERE" clause.
     *
     * @param collection the collection ob objects
     * @return the "IN" statement string
     */
    public String generateInParameters(Collection<? extends Object> collection) {
        StringBuilder inStatement = new StringBuilder(" IN ( ");
        int count = collection.size();
        for (int cv = 0; cv < count; cv++) {
            if (cv != 0) {
                inStatement.append(", ");
            }

            inStatement.append("?");
        }
        inStatement.append(" ) ");
        return inStatement.toString();
    }

    @Getter
    @Setter
    public static class QueryResults implements AutoCloseable {
        ResultSet results;
        PreparedStatement query;

        @Override
        public void close() {
            if (results != null) {
                try {
                    results.close();
                } catch (SQLException e) {
                    logger.log(Level.WARNING, e.getMessage(), e);
                }
            }

            if (query != null) {
                try {
                    query.close();
                } catch (SQLException e) {
                    logger.log(Level.WARNING, e.getMessage(), e);
                }
            }
        }
    }
}
