package com.ncitguys.enums.common;

public interface IntegerEnum extends DbEnum {
	
	@Override
	public abstract Integer getDbValue();
	
	@Override
	public abstract String getWord();
	
}
