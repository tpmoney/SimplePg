package com.ncitguys.enums.common;

public interface DbEnum {
	public Object getDbValue();
	public Object getWord();
}
