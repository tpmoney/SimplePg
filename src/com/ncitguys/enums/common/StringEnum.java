package com.ncitguys.enums.common;

public interface StringEnum extends DbEnum {
	
	@Override
	public String getDbValue();
	
	@Override
	public String getWord();
	
}
