package com.ncitguys.enums;

import com.ncitguys.enums.common.StringEnum;

public enum YesNoEnumType implements StringEnum{
	Yes("Y", "Yes"),
	No("N", "No")
	;

	private String dbValue;
	private String word;
	
	YesNoEnumType(String dbValue, String word){
		this.dbValue = dbValue;
		this.word = word;
	}
	
	@Override
	public String getDbValue() {
		return dbValue;
	}

	@Override
	public String getWord() {
		return word;
	}
	
}